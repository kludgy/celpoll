#if !defined(AFX_CELPOLLDLG_H__854FA1E5_3CEA_11D5_9005_00500404B176__INCLUDED_)
#define AFX_CELPOLLDLG_H__854FA1E5_3CEA_11D5_9005_00500404B176__INCLUDED_

class CCelpollDlg : public CDialog
{
public:
	int portopen;
	int polling;

	CCelpollDlg(CWnd* pParent = NULL);
	virtual ~CCelpollDlg();

	//{{AFX_DATA(CCelpollDlg)
	enum { IDD = IDD_CELPOLL_DIALOG };
	CEdit	ed_v250_ss_sid;
	CEdit	ed_v250_band;
	CEdit	ed_v250_bcs;
	CEdit	ed_v250_fer;
	CEdit	ed_v250_service;
	CProgressCtrl	pb_v250_bcl;
	CProgressCtrl	pb_v250_sqm;
	CEdit	ed_v250_sqm;
	CEdit	ed_v250_bcl;
	CEdit	ed_v250_fwrev;
	CEdit	ed_v250_model;
	CEdit	ed_v250_mfg;
	CEdit	ed_v250_esn;
	CButton	bn_config;
	CButton	bn_poll;
	CButton	bn_openport;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CCelpollDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	HICON m_hIcon;

	//{{AFX_MSG(CCelpollDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnConfig();
	afx_msg void OnOpenPort();
	afx_msg void OnPoll();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	void SetUIState(int state);
	void CopyQueryInfo();
	void SetDefaultQueryInfo();
	void PollEvent();
	void StartPolling();
	void StopPolling();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELPOLLDLG_H__854FA1E5_3CEA_11D5_9005_00500404B176__INCLUDED_)
