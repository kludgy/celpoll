#if !defined(AFX_CONFIGDLG_H__854FA1ED_3CEA_11D5_9005_00500404B176__INCLUDED_)
#define AFX_CONFIGDLG_H__854FA1ED_3CEA_11D5_9005_00500404B176__INCLUDED_

class CConfigDlg : public CDialog
{
public:
	CConfigDlg(CWnd* pParent = NULL);

	//{{AFX_DATA(CConfigDlg)
	enum { IDD = IDD_CONFIG };
	int		polldelay;
	CString	portname;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CConfigDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDLG_H__854FA1ED_3CEA_11D5_9005_00500404B176__INCLUDED_)
