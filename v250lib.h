typedef struct v250info_s 
{
	int bcs;		// Battery charge status 0-3:
					//		0 = powered by battery
					//		1 = external power
					//		2 = battery status n/a
					//		3 = recognized power fault

	int bcl;		// Battery charge level 0-100(%), only relevant when bcs=0

	int sqm;		// Signal quality measure 0-31 (0-100%),99:
					//		99 = not known or not detectable

	int fer;		// Frame error rate 0-7,99:
					//		0 = < 0.01%
					//		1 = [0.01%..0.1%)
					//		2 = [0.1%..0.5%)
					//      3 = [0.5%..1.0%)
					//		4 = [1.0%..2.0%)
					//		5 = [2.0%..4.0%)
					//		6 = [4.0%..8.0%)
					//		7 = >= 8.0%
					//		99 = not known or not detectable
					//		all other values reserved

	int service;	// Query analogue or digital service 0-3,4-255:
					//		0 = no service
					//		1 = cdma
					//		2 = tdma
					//		3 = analogue
					//		4..255 = reserved

	int ss_band;	// Serving system band 0-2:
					//		0 = no service
					//		1 = 800MHZ cellular
					//		2 = PCS 1900MHz

	char ss_ab;		// Serving system band

	int ss_sid;		// Serving system identifier:
					//		99999 = no service

	char mfg[256];	// manufacturer
	char model[64];	// model
	char fwrev[32];	// firmware revision
	char esn[50];	// serial number

} v250info_t;

extern int OpenV250(char *device);
extern void CloseV250(void);
extern int QueryV250(v250info_t *info);
extern int PollV250(v250info_t *info);



// TODO: dial button
// TODO: multithreading
// TODO: batch fetch
