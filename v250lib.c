#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "v250lib.h"

#define COMBUFSIZ BUFSIZ
#define CMDBUFSIZ 1024

static char		cmdbuf[CMDBUFSIZ];
static int		cmdi;
static char		iob[COMBUFSIZ];
static char		comname[8];
static HANDLE	com = INVALID_HANDLE_VALUE;

#define AT_OK 0
#define AT_ERROR 1
#define AT_TIMEOUT 2
#define AT_OVERFLOW 3

static int		flusherr	= AT_OK;
static char		*r_ok		= "OK\r\n";
static int		r_len_ok	= 4;
static char		*r_err		= "ERROR\r\n";
static int		r_len_err	= 7;

static int Flush(void)
{
	DWORD bytesread;

	cmdbuf[0] = 0;
	cmdi = 0;
	while(1)
	{
		ReadFile(com, iob, COMBUFSIZ-1, &bytesread, NULL);

		if(bytesread)
		{
			iob[bytesread] = 0;
			_snprintf(cmdbuf+cmdi, CMDBUFSIZ-cmdi, "%s", iob);
			cmdi += bytesread;

			if (cmdi >= CMDBUFSIZ)
				return AT_OVERFLOW;

			if (cmdi >= 4)
			{
				if (!stricmp(r_ok, cmdbuf+cmdi-r_len_ok))
					break;

				if (!stricmp(r_err, cmdbuf+cmdi-r_len_err))
					return AT_ERROR;
			}
		}
		else
			return AT_TIMEOUT;
	}

	return AT_OK;
}

static void FetchResult(char *cmd)
{
	DWORD byteswritten;

	if (flusherr != AT_OK)
		return;		// don't stomp old errors

	_snprintf(iob, COMBUFSIZ, cmd);
	WriteFile(com, iob, strlen(iob), &byteswritten, NULL);

	flusherr = Flush();
}

int QueryV250(v250info_t *info)
{
	flusherr = AT_OK;

	// are you there, modem?
	FetchResult("at\r");

	// manufacturer
	FetchResult("at+gmi\r");
	sscanf(cmdbuf, "at+gmi +GMI: %[^\r]", iob);
	strncpy(info->mfg, iob, sizeof(info->mfg)/sizeof(char));

	// model
	FetchResult("at+gmm\r");
	sscanf(cmdbuf, "at+gmm +GMM: %[^\r]", iob);
	strncpy(info->model, iob, sizeof(info->model)/sizeof(char));

	// firmware revision
	FetchResult("at+gmr\r");
	sscanf(cmdbuf, "at+gmr +GMR: %[^\r]", iob);
	strncpy(info->fwrev, iob, sizeof(info->fwrev)/sizeof(char));

	// esn
	FetchResult("at+gsn\r");
	sscanf(cmdbuf, "at+gsn +GSN: %[^\r]", iob);
	strncpy(info->esn, iob, sizeof(info->esn)/sizeof(char));

	return flusherr;
}

int PollV250(v250info_t *info)
{
	flusherr = AT_OK;

	// charge/battery level
	FetchResult("at+cbc?\r");
	sscanf(cmdbuf, "at+cbc? +CBC: %i,%i", &info->bcs, &info->bcl);

	// signal and call status
	FetchResult("at+csq?\r");
	sscanf(cmdbuf, "at+csq? +CSQ: %i,%i", &info->sqm, &info->fer);

	// service type
	FetchResult("at+cad?\r");
	sscanf(cmdbuf, "at+cad? +CAD: %i", &info->service);
	info->service = abs(info->service);

	// air interface data
	FetchResult("at+css?\r");
	sscanf(cmdbuf, "at+css? +CSS: %i,%c,%i", &info->ss_band, &info->ss_ab, &info->ss_sid);
	info->service = abs(info->service);

	return flusherr;
}

/*static void PrintV250(v250info_t *info)
{
	static char *service_names[] = {
		"No service",
		"CDMA",
		"TDMA",
		"Analogue"
	};

	printf(
		"Battery charge status....... %i\n"
		"Battery charge level........ %i%%\n"
		"Signal quality measure...... %i\n"
		"Frame error rate............ %i\n"
		"Service..................... %s\n\n"
		"Manufacturer................ %s\n"
		"Model....................... %s\n"
		"Firmware revision........... %s\n"
		"ESN......................... %s\n",
		info->bcs,
		info->bcl,
		info->sqm,
		info->fer,
		(info->service > 3) ? itoa(info->service, iob, 10) : service_names[info->service],
		info->mfg,
		info->model,
		info->fwrev,
		info->esn
	);
}*/

int OpenV250(char *device)
{
	DCB				dcb;
	COMMTIMEOUTS	ctout = { 0, 0, 100, 0, 0 };

	strncpy(comname, device, sizeof(comname)/sizeof(char));
	com = CreateFile(comname, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);

	if (com == INVALID_HANDLE_VALUE)
		return -1;

	SetCommTimeouts(com, &ctout);

	dcb.DCBlength = sizeof(DCB);
	GetCommState(com, &dcb);
	BuildCommDCB("19200,N,8,1", &dcb);
	SetCommState(com, &dcb);

	return 0;
}

void CloseV250()
{
	if (com)
	{
		CloseHandle(com);
		com = INVALID_HANDLE_VALUE;
	}
}
