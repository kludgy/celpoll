//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by celpoll.rc
//
#define IDD_CELPOLL_DIALOG              102
#define IDD_CONFIG                      105
#define IDR_MAINFRAME                   128
#define ID_PORTSEL                      1000
#define ID_V250_MFG                     1000
#define ID_V250_MODEL                   1001
#define ID_V250_ESN                     1002
#define ID_V250_FWREV                   1003
#define ID_POLL                         1004
#define ID_BATTERYVAL                   1007
#define ID_SIGNALVAL                    1008
#define ID_BATTERY                      1009
#define ID_SIGNAL                       1010
#define ID_SERVICE                      1011
#define ID_CALLSTAT                     1012
#define ID_OPENPORT                     1013
#define ID_CONFIG                       1014
#define ID_POWERSRC                     1015
#define ID_POLLDELAY                    1016
#define ID_BAND                         1016
#define ID_BANDLETTER                   1017
#define ID_NETWORKNUM                   1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
