#include "stdafx.h"
#include "celpoll.h"
#include "ConfigDlg.h"

CConfigDlg::CConfigDlg(CWnd* pParent)
	: CDialog(CConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigDlg)
	polldelay = 0;
	portname = _T("");
	//}}AFX_DATA_INIT
}

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Text(pDX, ID_POLLDELAY, polldelay);
	DDV_MinMaxInt(pDX, polldelay, 0, 999999);
	DDX_CBString(pDX, ID_PORTSEL, portname);
	DDV_MaxChars(pDX, portname, 4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
