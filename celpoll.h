#if !defined(AFX_CELPOLL_H__854FA1E3_3CEA_11D5_9005_00500404B176__INCLUDED_)
#define AFX_CELPOLL_H__854FA1E3_3CEA_11D5_9005_00500404B176__INCLUDED_

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

typedef struct {
	char portname[8];
	int polldelay;
} cfg_t;

extern cfg_t appcfg;
extern char *appiob;
extern int Alert(char *text, UINT type);
extern void WriteConfigFile();

class CCelpollApp : public CWinApp
{
public:
	CCelpollApp();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCelpollApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCelpollApp)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CELPOLL_H__854FA1E3_3CEA_11D5_9005_00500404B176__INCLUDED_)
