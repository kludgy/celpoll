#include "stdafx.h"
#include "celpoll.h"
#include "celpollDlg.h"

char *apptitle = "Cel Poll";
cfg_t appcfg = { "COM1", 1000 };
static char appiob_block[BUFSIZ];
char *appiob = appiob_block;

int Alert(char *text, UINT type)
{
	return MessageBox(AfxGetMainWnd()->m_hWnd, text, apptitle, type);
}

void WriteConfigFile()
{
	FILE *fh;

	fh = fopen("celpoll.cfg", "w");

	if (!fh)
	{
		Alert("Couldn't open celpoll.cfg for writing.\nChanges will not be saved.", MB_OK | MB_ICONERROR);
		return;
	}

	fprintf(fh, "portname %s\npolldelay %i\n", appcfg.portname, appcfg.polldelay);

	if (ferror(fh))
		Alert("Error writing to cellpoll.cfg.\nThe file may be corrupt.", MB_OK | MB_ICONERROR);

	fclose(fh);
}

static void ReadConfigFile()
{
	FILE *fh;
	int numfields;

	fh = fopen("celpoll.cfg", "r");

	if (!fh)
	{
		if (Alert("Couldn't open celpoll.cfg for reading.\n"
			  "Attempt to create a new file now?", MB_YESNO | MB_ICONWARNING) == IDYES)
		{
			WriteConfigFile();
		}

		// no need to reload since the config was written with the current settings
		return;
	}

	numfields = fscanf(fh, "portname %s\npolldelay %i\n", appiob, &appcfg.polldelay);
	
	if (numfields < 2)
		Alert("Not all of celpoll.cfg could be read.\n"
			  "The configuration may be incomplete.", MB_OK | MB_ICONWARNING);

	if (numfields >= 1)
		strncpy(appcfg.portname, appiob, sizeof(appcfg.portname)/sizeof(char));

	fclose(fh);
}

BEGIN_MESSAGE_MAP(CCelpollApp, CWinApp)
	//{{AFX_MSG_MAP(CCelpollApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

CCelpollApp::CCelpollApp()
{
}

CCelpollApp theApp;

BOOL CCelpollApp::InitInstance()
{
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CCelpollDlg dlg;
	m_pMainWnd = &dlg;

	ReadConfigFile();
	dlg.DoModal();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
