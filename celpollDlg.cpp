#include "stdafx.h"
#include "celpoll.h"
#include "celpollDlg.h"
#include "configdlg.h"
extern "C" { 
#include "v250lib.h" 
}

CCelpollDlg::CCelpollDlg(CWnd* pParent)
	: CDialog(CCelpollDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCelpollDlg)
	//}}AFX_DATA_INIT

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	portopen = 0;
	polling = 0;
}

CCelpollDlg::~CCelpollDlg()
{
	CloseV250();
}

void CCelpollDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CCelpollDlg)
	DDX_Control(pDX, ID_NETWORKNUM, ed_v250_ss_sid);
	DDX_Control(pDX, ID_BAND, ed_v250_band);
	DDX_Control(pDX, ID_POWERSRC, ed_v250_bcs);
	DDX_Control(pDX, ID_CALLSTAT, ed_v250_fer);
	DDX_Control(pDX, ID_SERVICE, ed_v250_service);
	DDX_Control(pDX, ID_BATTERY, pb_v250_bcl);
	DDX_Control(pDX, ID_SIGNAL, pb_v250_sqm);
	DDX_Control(pDX, ID_SIGNALVAL, ed_v250_sqm);
	DDX_Control(pDX, ID_BATTERYVAL, ed_v250_bcl);
	DDX_Control(pDX, ID_V250_FWREV, ed_v250_fwrev);
	DDX_Control(pDX, ID_V250_MODEL, ed_v250_model);
	DDX_Control(pDX, ID_V250_MFG, ed_v250_mfg);
	DDX_Control(pDX, ID_V250_ESN, ed_v250_esn);
	DDX_Control(pDX, ID_CONFIG, bn_config);
	DDX_Control(pDX, ID_POLL, bn_poll);
	DDX_Control(pDX, ID_OPENPORT, bn_openport);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCelpollDlg, CDialog)
	//{{AFX_MSG_MAP(CCelpollDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_CONFIG, OnConfig)
	ON_BN_CLICKED(ID_OPENPORT, OnOpenPort)
	ON_BN_CLICKED(ID_POLL, OnPoll)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static char *openport_labels[2] = {
	"Open &port",
	"Close &port"
};

static v250info_t celinfo;

#define UI_PORTCLOSED 0
#define UI_PORTOPEN   1
#define UI_POLLING    2

#define ID_POLLTIMER  1

void CCelpollDlg::SetUIState(int state)
{
	switch (state)
	{
	case UI_PORTOPEN:
		bn_openport.SetWindowText(openport_labels[1]);
		bn_config.EnableWindow(FALSE);
		bn_poll.EnableWindow(TRUE);
		bn_poll.SetCheck(0);
		break;

	case UI_POLLING:
		bn_poll.SetCheck(1);
		break;

	case UI_PORTCLOSED:
	default:
		bn_openport.SetWindowText(openport_labels[0]);
		bn_config.EnableWindow(TRUE);
		bn_poll.EnableWindow(FALSE);
		SetDefaultQueryInfo();
		break;

	}
}

BOOL CCelpollDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	memset(&celinfo, 0, sizeof(v250info_t));
	SetUIState(UI_PORTCLOSED);

	pb_v250_bcl.SetRange(0, 100);
	pb_v250_sqm.SetRange(0, 31);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCelpollDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CCelpollDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCelpollDlg::OnConfig() 
{
	CConfigDlg dlg;

	dlg.portname = appcfg.portname;
	dlg.polldelay = appcfg.polldelay;
	
	if (dlg.DoModal() == IDOK)
	{
		strncpy(appcfg.portname, dlg.portname, sizeof(appcfg.portname)/sizeof(char));
		appcfg.polldelay = dlg.polldelay;
		WriteConfigFile();
	}
}

void CCelpollDlg::OnOpenPort() 
{
	if (!portopen)
	{
		if (OpenV250(appcfg.portname))
		{
			_snprintf(appiob, BUFSIZ, "Couldn't open %s", appcfg.portname);
			Alert(appiob, MB_OK | MB_ICONERROR);
			return;
		}

		portopen = 1;
		SetUIState(UI_PORTOPEN);

		if (QueryV250(&celinfo))
		{
			_snprintf(appiob, BUFSIZ, "Comm failure: Couldn't query phone on %s.\nCheck connections and phone status.", appcfg.portname);
			Alert(appiob, MB_OK | MB_ICONWARNING);
		}
		else
			CopyQueryInfo();
	}
	else
	{
		StopPolling();
		CloseV250();
		portopen = 0;
		SetUIState(UI_PORTCLOSED);
	}
}

void CCelpollDlg::CopyQueryInfo()
{
	ed_v250_mfg.SetWindowText(celinfo.mfg);
	ed_v250_model.SetWindowText(celinfo.model);
	ed_v250_esn.SetWindowText(celinfo.esn);
	ed_v250_fwrev.SetWindowText(celinfo.fwrev);
}

void CCelpollDlg::SetDefaultQueryInfo()
{
/*	ed_v250_mfg.SetWindowText("(manufacturer)");
	ed_v250_model.SetWindowText("(model)");
	ed_v250_esn.SetWindowText("(serial no.)");
	ed_v250_fwrev.SetWindowText("(firmware rev.)");*/
}

void CCelpollDlg::PollEvent()
{
	static char *servicenm[] = {
		"No service",
		"Digital (CDMA)",
		"Digital (TDMA)",
		"Analogue"
	};

	static char *bandnm[] = {
		"No service",
		"800MHz Cellular",
		"1900MHz PCS"
	};

	static char *bcsnm[] = {
		"Battery",
		"External",
		"Status n/a",
		"Recognized power fault"
	};

	static char *fernm_d[] = {
		"< 0.01 %",
		"0.01..0.1 %",
		"0.1..0.5 %",
		"0.5..1.0 %",
		"1.0..2.0 %",
		"2.0..4.0 %",
		"4.0..8.0 %",
		">= 8.0 %"
	};

	static char *fernm_a[] = {
		"Analogue (0)",
		"Analogue (1)",
		"Analogue (2)",
		"Analogue (3)",
		"Analogue (4)",
		"Analogue (5)",
		"Analogue (6)",
		"Analogue (7)"
	};

	if (PollV250(&celinfo))
	{
		StopPolling();
		_snprintf(appiob, BUFSIZ, "Comm failure: Couldn't poll phone on %s.\nCheck connections and phone status.", appcfg.portname);
		Alert(appiob, MB_OK | MB_ICONWARNING);
		return;
	}

	// fill in statistics

	// battery
	if (celinfo.bcs == 0)
	{
		pb_v250_bcl.SetPos(celinfo.bcl);
		_snprintf(appiob, BUFSIZ, "%i%%", celinfo.bcl);
		ed_v250_bcl.SetWindowText(appiob);
	}
	else 
	{
		pb_v250_bcl.SetPos(0);
		_snprintf(appiob, BUFSIZ, "n/a");
		ed_v250_bcl.SetWindowText(appiob);
	}

	// powersrc
	if (celinfo.bcs < 4)
		_snprintf(appiob, BUFSIZ, bcsnm[celinfo.bcs]);
	else
		_snprintf(appiob, BUFSIZ, "(%i) Unknown code", celinfo.bcs);

	ed_v250_bcs.SetWindowText(appiob);

	// signal
	if (celinfo.sqm == 99 /* not known or not detectable */ || celinfo.service == 3)
	{
		pb_v250_sqm.SetPos(0);
		ed_v250_sqm.SetWindowText("n/a");
	}
	else
	{
		pb_v250_sqm.SetPos(celinfo.sqm);
		_snprintf(appiob, BUFSIZ, "%.0f%%", (float)(celinfo.sqm * 100) / 31.f);
		ed_v250_sqm.SetWindowText(appiob);
	}

	// frame error rate
	if (celinfo.fer == 99)
	{
		ed_v250_fer.SetWindowText("Not known or not detectable");
	}
	else if (celinfo.service == 3)
	{
		// analogue interpretation
		if (celinfo.fer < 8)
			ed_v250_fer.SetWindowText(fernm_a[celinfo.fer]);
		else
		{
			_snprintf(appiob, BUFSIZ, "(%i) Reserved", celinfo.fer);
			ed_v250_fer.SetWindowText(appiob);
		}
	}
	else
	{
		// digital interpretation
		if (celinfo.fer < 8)
			ed_v250_fer.SetWindowText(fernm_d[celinfo.fer]);
		else
		{
			_snprintf(appiob, BUFSIZ, "(%i) Reserved", celinfo.fer);
			ed_v250_fer.SetWindowText(appiob);
		}
	}

//	else if (celinfo.fer < 8)
//		ed_v250_fer.SetWindowText(fernm[celinfo.fer]);
//	else
//	{
//		_snprintf(appiob, BUFSIZ, "(%i) Reserved", celinfo.fer);
//		ed_v250_fer.SetWindowText(appiob);
//	}

	// service
	if (celinfo.service < 4)
		_snprintf(appiob, BUFSIZ, "%s", servicenm[celinfo.service]);
	else
		_snprintf(appiob, BUFSIZ, "(%i) Unknown code", celinfo.service);

	ed_v250_service.SetWindowText(appiob);

	// band
	if (celinfo.ss_band < 3)
		_snprintf(appiob, BUFSIZ, "%c %s", celinfo.ss_ab, bandnm[celinfo.ss_band]);
	else
		_snprintf(appiob, BUFSIZ, "%c Unknown (%i)", celinfo.ss_ab, celinfo.ss_band);

	ed_v250_band.SetWindowText(appiob);

	// sid
	if (celinfo.ss_sid == 99999)
	{
		ed_v250_ss_sid.SetWindowText("No service");
	}
	else
	{
		_snprintf(appiob, BUFSIZ, "%i", celinfo.ss_sid);
		ed_v250_ss_sid.SetWindowText(appiob);
	}
}

void CCelpollDlg::StartPolling()
{
	SetTimer(ID_POLLTIMER, appcfg.polldelay, 0);
	SetUIState(UI_POLLING);
	polling = 1;
}

void CCelpollDlg::StopPolling()
{
	KillTimer(ID_POLLTIMER);
	SetUIState(UI_PORTOPEN);
	polling = 0;
}

void CCelpollDlg::OnPoll() 
{
	if (polling)
		StopPolling();
	else
	{
		StartPolling();
		PollEvent(); // first poll is always immediate
	}
}

void CCelpollDlg::OnTimer(UINT nIDEvent) 
{
	PollEvent();
	CDialog::OnTimer(nIDEvent);
}
